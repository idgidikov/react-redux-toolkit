
import { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useDispatch,useSelector } from 'react-redux';
import {add} from '../store/cartSlice'
import {getProducts} from '../store/productSlice'
function Products() {
  
  const dispatch = useDispatch();
  const {data : products,status
   } = useSelector(state => state.products)

  useEffect(() => {
   dispatch(getProducts());
  }, []);

  const addToCart = (product) => {
    dispatch(add(product));
  }
  if(status=== "Loading"){
    return <p>Loading...</p>
  }
  if(status=== "error"){
    return <p>Something went wrong ! </p>
  }
  

  const cards = products.map((product, index) => {
    return (<div key={index} className="col-md-3" style={{ marginBottom: '10px' }}>
      <Card className='h-100'>
        <div className='text-center'>
          <Card.Img variant="top" src={product.image} style={{ width: '100px', height: '130px' }} />
        </div>

        <Card.Body className='text-center'>
          <Card.Title>{product.title}</Card.Title>
          <Card.Text >
            {product.price}
          </Card.Text>

        </Card.Body>
        <Card.Footer className='text-center' style={{background : 'white'}}>
          <Button variant="primary" onClick={()=> addToCart(product)}>Add To Card</Button>
        </Card.Footer>
      </Card>

    </div>)

  })
  return (<>
    <div>Products</div>
    <div className='row'>
      {cards}
    </div>


  </>


  )
}

export default Products