
import { useDispatch, useSelector } from 'react-redux';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import 'bootstrap/dist/css/bootstrap.min.css';
import {remove} from '../store/cartSlice'
function Cart() {
    const dispatch = useDispatch();
    const productCart = useSelector(state => state.cart);
    const removeToCart =(id) => {
        dispatch(remove(id));
    }
    const cards = productCart?.map((product, index) => {
        return (<div key={index} className="col-md-12" style={{ marginBottom: '10px' }}>
                <Card className='h-100'>
                    <div className='text-center'>
                        <Card.Img variant="top" src={product.image} style={{ width: '100px', height: '130px' }} />
                    </div>
    
                    <Card.Body className='text-center'>
                        <Card.Title>{product.title}</Card.Title>
                        <Card.Text >
                            {product.price}
                        </Card.Text>
    
                    </Card.Body>
                    <Card.Footer className='text-center' style={{ background: 'white' }}>
                        <Button variant="danger" onClick={() => removeToCart(product.id)}>Remove Item</Button>
                    </Card.Footer>
                </Card>
    
            </div>)
    })
    return (<>
        <div className='row'>
            {cards}
        </div>
        

    </>


    )
}

export default Cart