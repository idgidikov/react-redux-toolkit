
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css'
import Products from './components/Products'
import RootLayout from './components/RootLayout'
import {createBrowserRouter, createRoutesFromElements , Route, RouterProvider} from 'react-router-dom';
import Dashboard from './components/Dashboard';
import Cart from './components/Cart';
function App() {
const router = createBrowserRouter(createRoutesFromElements(
  <Route path="/" element = {<RootLayout/>}>
    <Route index element={<Dashboard/>}></Route>
    <Route path='/cart' element={<Cart/>}></Route>
  </Route>
))

  return (
    <>
      <div>
      <RouterProvider router={router}>

      </RouterProvider>
       
      </div>
      
    </>
  )
}

export default App
